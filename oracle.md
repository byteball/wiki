<!-- TITLE: Oracle -->
<!-- SUBTITLE: An ORACLE is a trusted third party that monitors specific external events and imports them into the Byteball database as a data feed. An example is a list of cryptocurrency exchange rates updated every ten minutes. -->
# Known oracles

**P2P exchange of Bytes vs. Bitcoin**  
FOPUBEUPBC6YLIQDLKL6EW775BMV7YOH  
See [Making P2P Great Again, Episode II: Bitcoin Exchange](https://medium.com/byteball/making-p2p-great-again-episode-ii-bitcoin-exchange-d98adfbde2a5)

**P2P random numbers gambling**  
FOPUBEUPBC6YLIQDLKL6EW775BMV7YOH (same oracle as above)  
See wiki article [Gambling](https://byteroll.com/gambling)
 
**Crypto exchange rates**  
JPQKPRI5FMTQRJF4ZZMYZYDQVRD55OTC  
See [Making P2P Great Again, Episode III: Prediction Markets](https://medium.com/byteball/making-p2p-great-again-episode-iii-prediction-markets-f40d49c0abab)


**Flight delay tracker for flight delays insurance**  
GFK3RDAPQLLNCMQEVGGD2KCPZTLSG3HN  
See [Making P2P Great Again, Episode IV: P2P Insurance](https://medium.com/byteball/making-p2p-great-again-episode-iv-p2p-insurance-cbbd1e59d527)

**Sports betting on soccer match results**   
TKT4UESIKTTRALRRLWS4SENSTJX6ODCW  
See wiki article [Trading, first section](https://byteroll.com/trading)

# Example of trading using an oracle

From the [Slack Byteball #prediction_markets forum](https://byteball.slack.com/messages/C4UMVU4QZ/details/):
> This is a trading floor, shout out your offers like in the old times. For example: 
> "I bet on BTC going below 1150 within 1 day, I pay 0.3GB out of 1GB"
> -- (tonych)

Use the crypto exchange rates oracle for this.

![Byteball 1](/uploads/slackjore/byteball-1.png "Byteball 1")
![Byteball 2](/uploads/slackjore/byteball-2.png "Byteball 2")

# Using the crypto-exchange-rates oracle in a smart contract
0. Practise thoroughly with small amounts of money first. The finished contract is human-readable but it is not obvious to a novice who gets to pay what under what conditions.
 
1. Pair up with a peer in wallet chat. Agree on the terms for the contract with your peer as needed throughout. After you have started writing the contract you will not be able to use the wallet chat until you finish (or abort) it. So either carefully agree all the details first, or have a separate chat channel open.

2. Peer sends you a receive address. Left-click the address. Click “offer a contract.” Go down filling in the easy bits, with the details as agreed on.

3. When you get to ORACLE ADDRESS, copy/paste in the crypto exchange rates one: 
> JPQKPRI5FMTQRJF4ZZMYZYDQVRD55OTC 

4. See [an example of the feed](https://explorer.byteball.org/#qjh31Ng5gSDDF5Vq+MizhVife3zw8eNFwBwSOcmnw3w=) from this oracle, and pick a currency pair, maybe BTC_USD. Then under DATA FEED NAME paste: 
> BTC_USD
 
5. Fill in the remaining easy bits with the agreed details, then click/press PAY AND OFFER. This will send your stake to the smart contract, and send a payment request to your peer for their stake.

**6. Caution: The peer should read the contract VERY carefully. Maybe neither party knows the other, and a costly mistake could have been made in the details, like a "<" (less than) sign instead of a ">" (greater than) sign. Or an incorrect currency pair. Also check that the payment request is for the correct amount, both the figures and the units (GB/MB).**

7. Your peer clicks Send on the payment request. The new contract will be visible as a smart wallet.

8. Now you both wait. You can view an up-to-date feed from that oracle by entering its full JPQ… address in the [byteball explorer search box](http://explorer.byteball.org). As soon as one party fulfills the terms of the contract its funds can be spent.

------

# Random numbers
0. Practise thoroughly with small amounts of money first. The finished contract is human-readable but it is not obvious to a novice who gets to pay what under what conditions.
 
1. Pair up with a peer in wallet chat. Agree on the terms for the contract with your peer as needed throughout. After you have started writing the contract you will not be able to use the wallet chat until you finish (or abort) it. So either carefully agree all the details first, or have a separate chat channel open.

2. Peer sends you a receive address. Left-click the address. Click “offer a contract.” Go down filling in the easy bits, with the details as agreed on.
  
3. When you get to ORACLE ADDRESS, copy/paste in the random-numbers-gambling one: 
> FOPUBEUPBC6YLIQDLKL6EW775BMV7YOH

4. Here is an example of the data feed from this oracle:

https://explorer.byteball.org/#Ns//o52EKR32ykuHfUfcm3ailHtta6TvM5kB07vbpVA=  
bitcoin_hash: 0000000000000000019dd457cf8e233701dab44ac855492d575a4366556213e0  
bitcoin_height: 460852  
bitcoin_merkle: GW9cQW5m0KLIi2suUlkzRP2yurg8jLEax3uHiun7kik=  
random460852:

You will need to know the current bitcoin height, and pick a future one to bet on. [Blockchain.info[(https://blockchain.info) is an easy source for current block height. Let's say you're doing this before the one above. So under DATA FEED NAME paste: 
> random460852
 
5. Fill in the remaining easy bits with the agreed details. Let's say you're doing an evens bet, above or below 50,000, then put "50000" in the POSTED VALUE box. Then click/press PAY AND OFFER. This will send your stake to the smart contract, and send a payment request to your peer for their stake.

**6. Caution: The peer should read the contract VERY carefully. Maybe neither party knows the other, and a costly mistake could have been made in the details, like a "<" (less than) sign instead of a ">" (greater than) sign. Or an incorrect bitcoin height. Also check that the payment request is for the correct amount, both the figures and the units (GB/MB).**

7. Your peer clicks Send on the payment request. The new contract will be visible as a smart wallet.

8. Now you both wait. You can view an up-to-date feed from that oracle by entering its full FOP… address in the [byteball explorer search box](http://explorer.byteball.org). Find the right one by the timing, compared to when "your" block was mined. As soon as one party fulfills the terms of the contract its funds can be spent.

-----
 **This "Oracle" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

