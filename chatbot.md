<!-- TITLE: Chatbot -->
<!-- SUBTITLE: You can pair your wallet with a bot, which emulates the experience of chatting with a live person. The bot will have very limited responses, but enough to get the job done. A merchant bot could allow you to select from a range of pizzas, for example, then pay for your order with two clicks while still in that chat session. -->
# Working chatbots
Byteball v.1.9.0 has a "bot store". Your hub provides the list. Default hub has 6 so far: Transition bot; Byte-BTC exchange; Flight delays oracle; Sports oracle; BTC oracle; Bounce bot.

## Just for fun
### The Byteball Bounce Bot V 1.1

Created by @vakar 2017-05-18

* Send some of your bytes, and they will be bounced back to you
* Use the bot to test your wallet settings and place test payments
* Read instructions carefully. 
* Send only bytes, don't send blackbytes.
* Make a Big Bounce and enter the all time hall of fame!

You find the bot in the Bot Store of your wallet.
<!--This [Byteball link](byteball:A6WUhxQX7bT1xY5UxoeL2/zNEgXGQXRO5ze4msXu3QrF@byteball.com.ar/bb#0000) will open up the bounce bot in your wallet. -->

<!--Or copy and paste the pairing code: A6WUhxQX7bT1xY5UxoeL2/zNEgXGQXRO5ze4msXu3QrF@byteball.com.ar/bb#0000-->

**Unfortunately the hub in Argentina is down, so you can't connect via the Bot Store.**
</br>Please temporarily use this pairing code: A6WUhxQX7bT1xY5UxoeL2/zNEgXGQXRO5ze4msXu3QrF@byteball.org/bb#0000

## Business
### Transition bot
After installing the wallet, [chat with the Transition Bot](byteball:A2WMb6JEIrMhxVk+I0gIIW1vmM3ToKoLkNF8TqUV5UvX@byteball.org/bb#0000) to participate in the next distribution round. That link will open automatically in your wallet when you click it.

### Byte-BTC Exchange
This is an **in-wallet chatbot** that provides a book for users. See the wiki article [Trading](https://byteroll.com/trading) for details.


-----
**This "Chatbot" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

