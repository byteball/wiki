<!-- TITLE: Trading blackbytes -->
<!-- SUBTITLE: How to buy or sell blackbytes in the wallet using conditional payments -->
# Actions to do
## The Slack #trading_blackbytes channel
Post "otc". This will automatically show the allowable bot commands. Post "asks" and "bids" to show the available offers and bids. Let's say you want to buy 20 GBB for 1GB. Post "bid 1 20". Assuming you used the correct format, the bookbot will respond "ok". Then post "bids" to check that your bid is actually what you wanted to post.

You need to find someone willing to sell/buy at your price. You might need to come down a bit. You might need to haggle. You can try DMing possible traders listed in the book.

0. Buyer and seller agree on amounts using some messaging channel such as Slack, email, even face to face.

## After agreeing the terms

1.  One party gives the other a pairing code, either written out in full or via QR code. Generate this in byteball wallet chat by clicking on "+ Add a new device" then "Invite the other device". It will look something like 
AhHPXCpCSTzD1CF53ELGTAsZ6MCA8Ogvk+koyibfPt/2v@byteball.org/bb#GxVBizZ2DSEf. The other party in byteball wallet chat clicks on "+ Add a new device" then "Accept invitation from the other device" and pastes the pairing code into the almost-invisible line above the 'pair' button.

2. Say hi etc to establish communication with the other party in the byteball chat window.

3. Buyer of GBB sends payment request (click on ". . ." to the left of the message text field) to seller, say for 20 GBB.  
-->Chat shows payment request from buyer for 20 GBB

4. Seller clicks on the payment request for 20 GBB and sends 20 GBB to smart wallet, binding it to the condition of receiving a payment of 1.0 GB (for example).  
-->Chat shows payment request from seller for 1.0 GB  
-->Chat shows payment from seller of 20 GBB

5. Buyer clicks on the payment request for 1.0 GB and sends payment for 1.0 GB.

6. Then go to the smart wallet, and send the funds to your main wallet.

# Notes

1. The new smart wallet won't show up until the payments confirm after 5 minutes or so.

2. If you just bought blackbytes then the smart wallet won’t have any (white)bytes in it yet to cover the sending fee.

3. You can see the details of the smart (wallet) contract by clicking the little eyeball to the right of the smart contract home screen.

4. Be careful to make payments from the correct wallet -- maybe you were looking in a smart wallet and forgot to get back into your main wallet.

5. You may get an error message if the funds you are trying to send haven't confirmed yet.


-----
**This "Trading blackbytes" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

