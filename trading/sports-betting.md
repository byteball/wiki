<!-- TITLE: Sports betting -->
<!-- SUBTITLE: Now you can P2P bet on the results of major soccer matches using smart contracts. See below for a list of the leagues covered.  -->
# Major soccer matches

## Before the end of the game
1. Using any communication channel, the punter/backer and "bookie"/layer agree the bet. Make sure it's covered by the oracle, i.e. that the match is this season and in one of the included leagues
2. In wallet chat, the backer sends the layer his receive address  
3. The layer left-clicks on it, clicks "offer a contract", carefully composes the contract, pays his stake and sends the contract    
4. The backer carefully checks the contract details are correct, both by eyeball and also by copy/paste searching in the data-feed-item verification list below   
5. If all OK, the backer then agrees the terms and pays his stake by clicking SEND 
6. The contract is now locked and the bet is irrevocable. 

## After the game

After the result is known, each peer can chat independently with the sports [oracle](https://byteroll.com/oracle) that will post the result of the game. This [Byteball link](byteball:Ar1O7dGgkkcABYNAbShlY2Pbx6LmUzoyRh6F14vM0vTZ@byteball.org/bb#0000) will open up the sports chatbot in your wallet automatically even if it is closed -- try it right now! As an example, the Arsenal-Sunderland match on 16 May will work. If the match isn't already in the database, it may take five minutes or more to get added.

The leagues covered by the oracle are:
* Champions League (UEFA)
* English National League 1
* English National League 2
* Liga Adelante (Spain)
* Primeira Liga (Portugal)
* Serie A (Italy)
* Serie B (Italy)

The backer can unlock the contract if his prediction was correct.  Otherwise, the layer can unlock the contract after the contract expires.

## Guide for composing the contract

1. Oracle address is TKT4UESIKTTRALRRLWS4SENSTJX6ODCW  
2. Data feed name is in the format HOMETEAM_AWAYTEAM_YYYY-MM-DD  
3. Remove common abbreviations from the team names such as FC, AS, etc  
4. Write team names in upper case  
5. Remove spaces from multi-word team names.  Manchester United becomes MANCHESTERUNITED  
6. Date of the match is in YYYY-MM-DD format  
7. Value of the feed is the name of the winning team formatted according to the same rules, or DRAW

You don't need to tell the bot the date of the match as it will find the most recent game between the two teams within the last 7 days.  When talking with the bot, type the names of the teams as they are commonly known (without FC, AS, etc).

## Source and screenshots

[Bitcointalk article 2017-05-20](https://bitcointalk.org/index.php?topic=1608859.msg19101028#msg19101028) 
# Verification lists
Carefully copy the data-feed item from the actual received contract, e.g. a soccer team, and paste/search (Ctrl-F etc) for it here on this page. If **your exact search item** does not appear as your electronic search result, even though by eyeball it seems to be here, then the "bookie" who wrote the contract will win the bet by default, whatever the actual result of the match.

##  Soccer oracle list (complete for 2016/7 season)

**Champions League (UEFA):**  
AJAX ALASHKERT ANDERLECHT APOEL ARSENAL ASTANA ASTRA ATHLÉTICO B36 BARCELONA BASEL BATE BAYERN BENFICA BEŞIKTAŞ CELTIC CLUBBRUGGE CRUSADERS CRVENAZVEZDA CSKAMOSKVA DINAMOTBILISI DINAMOZAGREB DORTMUND DUDELANGE DUNDALK DYNAMOKYIV SANTACOLOMA FCSB FENERBAHÇE FERENCVÁROS FH FLORA HAPOELBEERSHEVA JUVENTUSTURIN KØBENHAVN LEGIA LEICESTER LEVERKUSEN LIEPĀJA LINCOLN LUDOGORETS LYON MANCHESTERCITY LADOSTPODGORICA MONACO MÖNCHENGLADBACH NAPOLI NORRKÖPING OLIMPIJALJUBLJANA OLYMPIACOS PAOK PARIS PARTIZANI PLZEŇ PORTO PSV QARABAĞ REALMADRID ROMA ROSENBORG ROSTOV SALZBURG SEVILLA SHAKHTAR SHERIFF SJK SPARTAPRAHA SPORTINGCP TNS TOTTENHAMHOTSPUR TREPENNE TRENČÍN VALLETTA VARDAR VILLARREAL YOUNGBOYS ŽALGIRIS ZRINJSKI

**English National League 1:**  
ARSENAL BOURNEMOUTH BURNLEY CHELSEA CRYSTALPALACE EVERTON HULLCITY LEICESTERCITY LIVERPOOL MANCHESTERCITY MANCHESTERUNITED MIDDLESBROUGH SOUTHAMPTON STOKECITY SUNDERLAND SWANSEACITY TOTTENHAMHOTSPUR WATFORD WESTBROMWICHALBION WESTHAMUNITED 

**English National League 2:**  
ACCRINGTON BARNET BLACKPOOL CAMBRIDGEUNITED CARLISLE CHELTENHAM COLCHESTER CRAWLEYTOWN CREWE DONCASTER EXETER GRIMSBY HARTLEPOOL LEYTONORIENT LUTON MANSFIELD MORECAMBE NEWPORT NOTTSCOUNTY PLYMOUTH PORTSMOUTH STEVENAGE WYCOMBE YEOVIL

**Liga Adelante (Spain):**  
ALCORCON ALMERIA CADIZ CORDOBA ELCHE GETAFE GIMNASTIC GIRONA HUESCA LEVANTE LUGO MALLORCA MIRANDES NURMANCIA RAYOVALLECANO REALOVIEDO REUS SEVILLAATLETICO TENERIFE UCAMMURCIA VALLADOLID ZARAGOZA

**Primeira Liga (Portugal):**  
AROUCA BELENENSES BENFICA BOAVISTA CHAVES ESTORIL FEIRENSE MARITIMO MOREIRENSE NACIONAL PAÇOSDEFERREIRA PORTO RIOAVE SPORTINGBRAGA SPORTINGCP TONDELA VITÓRIAGUIMARÃES VITÓRIASETÚBAL

**Serie A (Italy):**  
ATALANTA BOLOGNA CAGLIARI CHIEVO CROTONE EMPOLI FIORENTINA GENOA INTERNAZIONALE JUVENTUS LAZIO MILAN NAPOLI PALERMO PESCARA ROMA SAMPDORIA SASSUOLO TORINO UDINESE

**Serie B (Italy):**  
ASCOLI AVELLINO BARI1908 BENEVENTO BRESCIA CARPI CESENA CITTADELLA FROSINONE HELLASVERONA LATINA NOVARA PERUGIA PISA PROVERCELLI SALERNITANA SPAL SPEZIA TERNANA TRAPANI VICENZA VIRTUSENTELLA



-----
**This "Sports betting" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)

