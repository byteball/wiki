<!-- TITLE: Slack -->
<!-- SUBTITLE: Some of the Byteball channels at Slack.com -->

# Our Slack channels
## #byteball_wiki
for matters concerning this wiki. The wiki editors/admins mostly hang out on Slack, and can be pinged easily.
## #general
for general matters

## #helpdesk
tends to be for newbies with simple questions, most of which should be able to be answered by giving a reference to a page in this wiki.

## #ideas
for brainstorming new stuff. "Sticky" ideas might rate their own section or page here. . . .

### Change the unit from GB to MB
You can choose the unit in your wallet as KB, MB or GB. Plus . . . 
> **tonych** [2017-05-24 10:48 PM] 
I don't think it would be a good move now.  We are still traded mostly against bitcoin and the price of MB (0.00013 BTC) with its many zeros wouldn't be easier to read than the price of GB 0.13 BTC

## #marketing
for questions, announcements, ideas, co-ordination on marketing matters

## #prediction_markets
for discussion about, and finding partners for, betting, trying out smart-contracts etc
## #tech
where the developers hang out

## #trading
for Byteball trading generally, discussion about troubles with external exchanges dealing with GBytes, etc

## #trading_blackbyte
limited to trading blackbytes and related discussions. There's a bookbot that lists asks and bids, with commands that are available displayed by posting "otc".