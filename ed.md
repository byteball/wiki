<!-- TITLE: Ed -->
<!-- SUBTITLE: This is a page for wiki editors or wiki editing. The system won't allow a regular wiki page with the name "Editors" or "Editing" though. -->
# Editor/editing info
If you look OK to an admin, you can get write access to the wiki. Register an account here then ask in the Slack #helpdesk channel. It helps to have some posting history on Slack.

## Article guidelines
See the Byteroll wiki page [Article guidelines](https://byteroll.com/ed/article-guidelines)

## Talk
Use the #byteball_wiki channel at the external [Byteball Slack](https://byteball.slack.com). That should work fine for our purposes.

## Sandbox
This is for experimenting with things rather than using a regular page. The idea is to clean it (apart from the top "sticky" note) at the end of each session so someone else gets a clean sandbox.

See the Byteroll wiki page [Sandbox](https://byteroll.com/ed/sandbox)

## Personal pages
These are for your own notes of things to do, half-written sections/articles not yet ready for a regular page, etc. Create your own page if you want one, but get the path right, namely "/ed/yourname". Maybe use your Slack name, and list it here so other editors can find you.

[Ed/Slackjore](https://byteroll.com/ed/slackjore)

[Ed/markcross](https://byteroll.com/ed/markcross)


# Wiki/article history
As of now, there is no user-friendly page history to see who wrote what when. But there is an available [Gitlab repository](https://gitlab.com/byteball/wiki/tree/master) for the technically-minded.



-----
**This "Ed" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)


# temporary stash of slackjore page as system has lost original and doesn't seem to allow new page creation

# About me
Professionally: I'm 65+, retired and my time is mostly my own. My nature: archivist, librarian, teacher, empowerer, that sort of thing.
# My notes
## "Nuked" pages to rename, re-path, for any new articles being created

 ...


## Articles needing work
**/interviews** needs more content  


## Additional keyboard shortcuts
**Newline:** Type space space at the end of the line above  
`monospace` (see page source for syntax)

## Table testing
Basic key commands

ENTER or double click — to start editing a cell  
ESC — to stop editing a cell  
TAB or Arrow Keys — to select a different cell  
CTRL+Z — to undo a change  
CTRL+Y — to redo a change  
Left mouse button — click a cell to select it (hold left button pressed to select adjacent cells)  
Hold CTRL to select multiple cells (not necessarily adjacent). 

Jore -- get this code using markdown table generator at http://www.tablesgenerator.com/markdown_tables

NOTE: Looks useful only for narrowtables. A wide table may be fine on desktop but it messes up on a mobile screen. Table below is about max possible width for a mobile:

| For GB     | Jun    | Jul    | Aug    | Sep    | Oct    | Nov    |
|-----------|--------|--------|--------|--------|--------|--------|
| **Jun**       | 0.0625 | 0.075  | 0.09   | 0.108  | 0.1296 | 0.1555 |
| **Jul**       |        | 0.0625 | 0.075  | 0.09   | 0.108  | 0.1296 |
| **Aug**       |        |        | 0.0625 | 0.075  | 0.09   | 0.108  |
| **Sep**       |        |        |        | 0.0625 | 0.075  | 0.09   |
| **Oct**       |        |        |        |        | 0.0625 | 0.075  |
| **Nov**       |        |        |        |        |        | 0.0625 |
| **Totals**    | **0.0625** | **0.1375** | **0.2275** | **0.3355** | **0.4651** | **0.6206** |

-----


# Witness
# from glossary
**WITNESS:** A witness is a highly reputable user with a real-world identity, who stamps each transaction seen. There are 12 witnesses involved in every transaction. In exchange for the work involved, a witness collects part of the transaction fee. This list varies very little from transaction to transaction. If 11 witnesses say no to a bad transaction and 1 says yes, that witness gets deemed unreliable and effectively fired. It would be unthinkable for all 12 to collude and allow a fraudulent transaction through. In this way the network is safeguarded.  
Ref [Byteball white paper PDF](https://byteball.org/Byteball.pdf)

# DAG-related terms needed for understanding of wp
**BEST PARENT:** *One way to build a main chain is to develop an algorithm that, given all parents of a unit, selects one of them as the “best parent”. The selection algorithm should be based only on knowledge available to the unit in question, i.e. on data contained in the unit itself and all its ancestors. Starting from any tip (a childless unit) of the DAG, we then travel backwards in history along the best parent links. Traveling this way, we build a main chain and eventually arrive at the genesis unit. Note that the main chain built starting from a specific unit will never change as new units are added. This is because on each step we are traveling from child to parent, and an existing unit can never acquire new parents.*

**EDGE:** A **link** connecting two units (vertexes) in a DAG. "Edge" and "vertex" are familiar enough with regard to a polygon, but "edge" is not at all intuitive to spot in a DAG, and for a while I wondered if it was some special kind of vertex.

**MAIN CHAIN** (Abbr. **MC**) *[W]e could choose a single chain along child-parent links within the DAG, and then relate all units to this chain. All the units will either lie directly on this chain, which we’ll call the main chain, or be reachable from it by a relatively small number of hops along the edges [links] of the graph. It’s like a highway with connectingside roads*

**MAIN CHAIN INDEX:** (Abbr. **MCI**)

**NON-SERIAL (UNIT):**

**SERIAL (UNIT):** *There is one more protocol rule that simplifies the definition of total order. We require, that if the same address posts more than one unit, it should include (directly or indirectly) all its previous units in every subsequent unit, i.e. there should be partial order between consecutive units from the same address. In other words, all units from the same author should be serial. If someone breaks this rule and posts two units such that there is no partial order between them (nonserial units), the two units are treated like double-spends even if they don’t try to spend the same output.*

**TIP:** Childless unit

**TOTAL ORDER** (Is this even a techni8cal term?) *Once we have a main chain (MC), we can establish a total order between two conflicting nonserial units. Let’s first index the units that lie directly on the main chain. The genesis unit has index 0, the next MC unit that is a child of genesis has index 1, and so on traveling forward along the MC we assign indexes to units that lie on the MC. For units that do not lie on the MC, we can find an MC index where this unit is first included (directly or indirectly). In such a way, we can assign an MC index (MCI) to every unit.*


## in more detail, how this done
Is this true? 1. ordinary user authors a storage unit, including regular fees

2. Unit is transmitted to all hubs, with priority to whichever one is stated in his wallet as the default

3. Hub broadcasts unit to be picked up by all 12 witnesses, and it's ok if up to 5 of them are offline/not working etc

4. Each witness who gets it validates/rejects it, and re-broadcasts it but with his stamp of approval attached as a child unit, a witness-approval-stamp unit

5. When a unit is observed to have a majority of witness approvals, 7 say, it gets the FINAL (confirmed) stamp



### dag-level nitty gritty, but NOT copypasta
Here's PART of the copypasta version to make nondev-readable from the white paper abstract:

> Total order is established by selecting a single chain on the DAG (the main chain) that is attracted  to units signed by known users called witnesses. 

> A unit whose hash is included earlier on the main chain is deemed earlier on the total order. Users choose the witnesses by naming the user-trusted witnesses in every storage unit.

> Witnesses are reputable users with real-world identities, and users who name them expect them to never try to double-spend. As long as the majority of witnesses behave as expected, all double-spend attempts are detected in time and marked as such. As witnesses-authored units accumulate after a user’s unit, there are deterministic (not probabilistic) criteria when the total order of the user’s unit is considered final.


# Statistics page
# GByte price
See widget on wiki front page in the navigation bar, powered by [Coinmarketcap](https://byteroll.com/coinmarketcap.com). If your have a narrow screen, and the nav bar doesn't show, click the link and scroll down.

# 
