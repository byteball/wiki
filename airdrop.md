<!-- TITLE: Airdrop -->
<!-- SUBTITLE: How and when the Byteball currencies of bytes and blackbytes get distributed -->
# Install a wallet
Normal users should choose a light/quick wallet, not the full/slow option. The full one takes a long time to sync, and you cannot change from full to light but have to start from scratch. Be warned.

See normal installation videos etc at the wiki article [Installation](https://byteroll.com/installation) 
# After installing a wallet
After installing, chat with the Transition Bot to participate in the next distribution round. [This link](byteball:A2WMb6JEIrMhxVk+I0gIIW1vmM3ToKoLkNF8TqUV5UvX@byteball.org/bb#0000) will open automatically in your wallet when you click it.

## Byteball address (disambiguation)
* A wallet address is all upper case, looks like K7RMH5EFPZW67JTS5B5GA6PDZA4MYX4LY  
* A unit address, like a transaction you sent, looks like Ip241kkFRkZnaVF61Z6+/JW3YELOaHn9C6PAjeMp8fs=  
* And a bitcoin address looks like 13AM4VW2dhxYgXeQepoHkHSQuy6NgaEb94

## Proving ownership 
The Transition Bot will ask you to prove ownership of a regular Bitcoin address you provide by either sending a small transaction from it, or by signing a message using its private key. Workable (free!) signing solutions are:
* Electrum  
* Trezor  
* Nano Ledger S
* Mycelium (on android)

Note that the bitcoin address can have zero bitcoins in it. The bot only needs a signature from the correct private key. Fractional bitcoin amounts like 0.027 BTC are fine too.
### BTC on exchanges
Generally it won't work to try and link a Byteball address to bitcoins held on an exchange. You need to know the private key to sign a message for the Transition bot. There apparently is an exception:

> cryptkeeper [2017-06-06 8:07 AM]  
@xxxx I stand corrected: cryptox.pl will pay out distributed bytes and blackbytes for a fee of 5%
## Multiple addresses
You are allowed to link more than one Bitcoin address to a single Byteball address. But you are not allowed to link more than one Byteball address to a single Bitcoin address. 
These are OK, for example:
| Byteball address | Bitcoin address |
|------------------|-----------------|
| CDE...           | 1j4y...         |
| CDE...           | 17g6...         |
| PQR...           | 1kmr...         |
| . . .            | . . .           |
# Distribution
All the Byteball (native) currency units were created at the outset. About 20% have been distributed so far, in five rounds, starting December 25, 2016. A snapshot recording which wallets will get "bytten" now gets taken every full moon, with the actual distribution occurring within a day or two.
# Current rules and rates

The snapshot for the 6th round will be taken at the full moon of June, on June 9, 2017 at 13:10 UTC. If you linked your BTC address(es) correctly, proving your balance at that moment in time, you will get delivered to your full or light wallet:

* **BTC to bytes:** For every 1 BTC, 62.5 MB [0.0625 GB or 62,500,000 (white)bytes] 

* **BTC to blackbytes:** For every 1 BTC, 2.1111 x 62.5 million blackbytes (money supply of blackbytes is 2.1111 times as much as that of bytes)
 
* **Bytes to bytes:** For every 10 (white)bytes on **any** Byteball address, 2 new (white)bytes 
 
* **Bytes to blackbytes:** For every 10 (white)bytes on a **linked** Byteball address: 4.2222 new blackbytes

## Further months?
If you did it right and got the free bytes/blackbytes, it will continue to work for the following months without you needing to do anything, as long as you don't move the bitcoins or the bytes in the linked (proven by Transition Bot) addresses.

## Projection using above figures (but exact number of months unknown)
### For every 1 BTC, BTC to bytes and bytes to bytes, 20% increase per month

| For GB     | Jun    | Jul    | Aug    | Sep    | Oct    | Nov    |
|-----------|--------|--------|--------|--------|--------|--------|
| **Jun**       | 0.0625 | 0.075  | 0.09   | 0.108  | 0.1296 | 0.1555 |
| **Jul**       |        | 0.0625 | 0.075  | 0.09   | 0.108  | 0.1296 |
| **Aug**       |        |        | 0.0625 | 0.075  | 0.09   | 0.108  |
| **Sep**       |        |        |        | 0.0625 | 0.075  | 0.09   |
| **Oct**       |        |        |        |        | 0.0625 | 0.075  |
| **Nov**       |        |        |        |        |        | 0.0625 |
| **Totals**    | **0.0625** | **0.1375** | **0.2275** | **0.3355** | **0.4651** | **0.6206** |

### For every 1 BTC, BTC to blackbytes and bytes to blackbytes, 40% increase per month

| For GBB | Jun    | Jul    | Aug    | Sep    | Oct    | Nov    |
|--------|--------|--------|--------|--------|--------|--------|
| **Jun**       | 0.1319 | 0.1846 | 0.2585 | 0.3619 | 0.5067 | 0.7093 |
| **Jul**       |        | 0.1319 | 0.1846 | 0.2585 | 0.3619 | 0.5067 |
| **Aug**       |        |        | 0.1319 | 0.1846 | 0.2585 | 0.3619 |
| **Sep**       |        |        |        | 0.1319 | 0.1846 | 0.2585 |
| **Oct**       |        |        |        |        | 0.1319 | 0.1846 |
| **Nov**       |        |        |        |        |        | 0.1319 |
| **Totals**    | **0.1319** | **0.3165** | **0.575**  | **0.9369** | **1.4436** | **2.1529** |

### Excluding BTC, and starting with 1 GB
| 1 GB | Jun     | Jul    | Aug    | Sep    | Oct    | Nov    |
|------|---------|--------|--------|--------|--------|--------|
| **GB**   | 1.2     | 1.44   | 1.728  | 2.0736 | 2.4883 | 2.986  |
| **GBB**  | 1.42222 | 2.0226 | 2.8766 | 4.091  | 5.818  | 8.2749 |

# Verify your balances
## Bitcoins
If you spend money from a bitcoin address, the "change" by default goes into a different address. Check before the airdrop that your bitcoins are in the linked address, and move them back there if necessary. Allow enough time for bitcoin transaction delays.

## Bytes
Similarly, if you've been using your byteball wallet even for small amounts, a big chunk of your bytes may have migrated to different addresses. Send them back to your linked address in time for the airdrop.

 ## The transition bot will tell you your linked balances
Link will open automatically in your wallet when you click it:  
[Transition Bot](byteball:A2WMb6JEIrMhxVk+I0gIIW1vmM3ToKoLkNF8TqUV5UvX@byteball.org/bb#0000) 

### Online check
You can also track linking progress at [Byteball main site: transition pages](https://transition.byteball.org).

# Just for fun, an old ditty rewritten
> Captive bytes at a linked address?  
The next-moon’s drop will byte 'em;  
Byttten bytes get further bytes  
And so ad infinitum.

(well, until the end of the year maybe)


-----
**This "Airdrop/Distribution" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
