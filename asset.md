<!-- TITLE: Asset -->
<!-- SUBTITLE: New assets/tokens created on the Byteball platform, this article not too technical -->
**"Asset" in financial community = "token" in non-financial community.**
# From white paper
> Users can issue new assets and define rules that govern their transferability. The rules can include spending restrictions such as a requirement for each transfer to be cosigned by the issuer of the asset, which is one way for financial institutions to comply with existing regulations. Users can also issue assets whose transfers are not published to the database, and therefore not visible to third parties. Instead,the information about the transfer is exchanged privately between users, and only a hash of the transaction and a spend proof (to prevent double-spends) are published to the database.

Ref [White paper abstract](https://byteball.org/Byteball.pdf)

# Byteball Asset Manager
1. Order your new asset from https://byteball.market.
2. Its name will be the address of the unit that sent it, e.g. ZW1b.... No custom names or fancy rules yet.
3. Let's say you are selling 100 Zwibs to a peer for 75 MB. In chat, peer sends you a receive address.
4. You left-click the address, and click "Pay to this address".
5. From the drop-down menu, select Zwibs. Click "bind this payment to a condition". Click "I receive another payment" as the condition. Fill in the amount of 75 MB. Click "Bind payment".
6. Fill in your payment amount of 100 Zwibs and click Send. Or click Cancel if you were just trying it out.
7. Peer reads the contract and if it's what was agreed, clicks Pay/Send. Done deal, both payments occur at once. 

## New assets
* Price = Buy/sell per unit. So 1 Zwib costs about 100 KB as a special introductory price.  
* Clicking the link will take you to the unit page, where you can see how the asset is defined.

| Asset |  Price | Contact | Notes |
|-------|------|-------|---------|-------|
| [Zwibs](https://explorer.byteball.org/#ZWlbZhl6C9IsV3biDC+ngzsLq58mQ0hbwWyLBLpFLQc=) |  105/95 KB | @slackjore | token is not really for sale with real funds   |
| [CK1](https://explorer.byteball.org/#3kc7H8A2oiWr9mv7AcWJeLCA0Cp8c3BLK04kYQ+5pfU=)  |  332/310 B    | @cryptkeeper     | community asset with membership-like features like special discounts at webshops   |
| [TIG](https://explorer.byteball.org/#LDpQQ0SpoyxqTHnaFpTYt/VJzeUhwDFwVEXBWbqq1AE=)   |  15/13 B    | @btcmacroecon      | first crypto asset designed to save the tigers from extinction    |
| [Hi's](https://explorer.byteball.org/#Hi/WFGThLixhyaPALYgKnONBgjid0+mAPXQB8hRWp+E=) | ... | @kiracomp | first Chinese new asset |
## Just do it
You can simply send your new (unrestricted) assets to **any** Byteball address, with no chat or contract needed. Copy the address, open the send tab, find the asset in your drop-down box, paste the address, enter the amount (in whole numbers), and click Send. Perfect for confusing people who aren't expecting to receive 25 Zingos in their wallet!

Caution: Be careful to send the right asset. Don't send someone 4 GB by mistake when you mean to send 4 Zingos. Not joking. Double-check.
## Transaction fees
Usual transaction fees apply. So sending 9 Zingos somewhere will cost 500 bytes, or whatever. At 1GB = $600, 1MB = $0.60, 1KB = $0.0006, and 500 bytes = $0.0003 so you won't go broke.

## Change address
Let's say you're new at this. You linked 1 BTC to your wallet, and you got 62,500,000 bytes at the last moondrop, and those are your only bytes. If someone sends you a few Zwibs, and you later send someone a Zwib, the transaction fee -- maybe 429 bytes or something -- will come out of your stash. The entire 62,500,000 will empty out of the address they were sitting in, 429 will be sent to wherever transaction fees go, and the 62,499,571 bytes "change" will return to a different address, a change address, in your wallet. This will have two effects, three if you count getting confused:

* You will see an "incoming" transaction for 62.5 MB, which will later show in your wallet history as MOVED  
* Until the payment clears you will have no spendable bytes, so will not be able to pay any more transaction fees for a few minutes.
# Bitcointalk copypasta
Ref [https://bitcointalk.org](https://bitcointalk.org/index.php?topic=1608859.0)

5/29 from tonych:
> Developer guide for issuing new assets on Byteball:  
> https://github.com/byteball/byteballcore/wiki/Issuing-assets-on-Byteball

5/30 response:  
> It's the same purpose as ethereum tokens, except the feature is built natively in Byteball and you don't need to write a smart-contract to manage the assets you emitted. 

5/30 from tonych (post #8739):

> [Examples] ICOs, shares, bonds, fiat-pegged coins, loyalty points, minutes of airtime, assets in online games, whatever you can imagine.

> [How traded and where] They can be immediately traded P2P via smart contracts (same as blackbytes).
Also we have a trustless exchange already prepared https://github.com/byteball/byteball-exchange but not started yet.


-----
**This "Asset" page main author: @slackjore at the** [Byteball slack](http://slack.byteball.org/)
