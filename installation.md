<!-- TITLE: Installation -->
<!-- SUBTITLE: Beginners' guide to installing the wallet and linking to BTC addresses before next airdrop -->
# Installation videos
Installation is pretty straightforward.

[Windows](https://byteroll.com/installation#windows)

[Mac OSX](https://byteroll.com/installation#mac)

[Linux](https://byteroll.com/installation#linux)


## Windows

[Basic overview of the complete installation process](https://www.youtube.com/watch?v=bBuLgihIpNA){.youtube}

## Mac OSX
coming . . . 

## Linux



### #1 Installing Electrum

[video](https://www.youtube.com/watch?v=gNxch7y-mTI){.youtube}


So from the [Electrum Ubuntu/Debian install](https://electrum.org/#download)
I would recommend the following line:

Note that for Ubuntu 16.04 an additional dependency appears to be necessary

```text
sudo apt-get install python-qt4 python-pip python-setuptools
```

### #2 Making the addresses visible

[Making Electrum bitcoin addresses visible](https://www.youtube.com/watch?v=JRa00eC5HNA){.youtube}


### Part 3 Signing


[Signing the wallet address with your bitcoin wallet](https://www.youtube.com/watch?v=6SpomFAX0GU&t=4s){.youtube}


-----
**This "Installation" page main author**: markcross on the [byteball slack](http://slack.byteball.org/)
