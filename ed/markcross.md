<!-- TITLE: Markcross -->
<!-- SUBTITLE: A quick summary of Markcross -->

# Keen ByteBall fan - based in the UK
Started writing assembler around 12 years old on ZX81, other computers I was exposed to early on the TRS-80, Apple II, ZX Spectrum, BBC Model B, RML 80, 286, 386, Atari, PrimeOS on last mainframe in UK education.

More to follow...

# Slackjore edit in progress...
Whilst many can assume installation is straightforward, many people are not only new to cryptographic technology but new to Linux.

Many people on the byteball slack have been experimenting with blockchain technologies for a number of years and are likely to recommended exploring Linux as your operating system of choice.

On the grounds that you are your own bank you could do well to really understand all the principles of security and being responsible for backing up **your** wallet.

Here are a few rapidly put together youtube videos that may help beginners. Please note I hope to improve upon these, like with a sound track!

[Installing on Linux](https://byteroll.com/installation#if-you-are-on-linux)

[Installing on Windows](https://byteroll.com/installation#if-you-are-on-windows)

[Part 1 Installing Electrum on Linux for the Airdrop binding process](https://byteroll.com/installation#part-1-installing-electrum-on-linux-for-the-airdrop-binding-process)

[Part 2 of airdrop signing the wallet address with Electrum - making the addresses visible](https://byteroll.com/installation#part-2-of-airdrop-signing-the-wallet-address-with-electrum-making-the-addresses-visible)

[Part 3 finally signing the wallet address with Electrum](https://byteroll.com/installation#part-3-finally-signing-the-wallet-address-with-electrum)

### If you are on Linux

[Install byteball on linux & how far is the sync done?](https://www.youtube.com/watch?v=tvGv29SLUkc){.youtube}

[Sync checking code page](https://pastebin.com/7nKQNhbE)

### If you are on Windows

[Basic overview of the complete installation process](https://www.youtube.com/watch?v=bBuLgihIpNA){.youtube}

### Part 1 Installing Electrum on Linux for the Airdrop binding process
[video](https://www.youtube.com/watch?v=gNxch7y-mTI){.youtube}


So from the [Electrum Ubuntu/Debian install](https://electrum.org/#download)
I would recommend the following line:

Note that for Ubuntu 16.04 an additional dependency appears to be necessary

```text
sudo apt-get install python-qt4 python-pip python-setuptools
```

### Part 2 of airdrop signing the wallet address with Electrum - making the addresses visible

This video is applicable to either a Linux or Windows installation and very probably Mac OS as well

[Making Electrum bitcoin addresses visible](https://www.youtube.com/watch?v=JRa00eC5HNA){.youtube}

### Part 3 finally signing the wallet address with Electrum

This video is applicable to either a Linux or Windows installation and very probably Mac OS as well

[Signing the wallet address with your bitcoin wallet](https://www.youtube.com/watch?v=6SpomFAX0GU&t=4s){.youtube}


-----
**This "Installation" page main author**: markcross on the [byteball slack](http://slack.byteball.org/)