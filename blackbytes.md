<!-- TITLE: Blackbytes -->
<!-- SUBTITLE: One of the two Byteball currencies. When you want complete privacy, pay in blackbytes, a cash-like untraceable currency whose transactions are not visible on the public database. They are sent peer-to-peer instead. -->
## Blackbytes

Blackbytes is the name of one, the first, defined private asset in Byteball. Byteball supports user-defined assets, the assets has properties such as private or public, amount of coins, coin-denominations and if any authentifiers is required for transferring, or if the asset is transferrable.

Blackbytes are defined as a private asset, meaning its never posted to the public Byteball database, it is ever only transferred when Byteball wallets have paired, private assets are sent encrypted through the Byteball network from device address to device address. When used over Tor, blackbytes become anonymous, and only the device-address can be known to the Byteball network.

To verify that a peer has not sent the same blackbyte to another peer before attempting to trick you into accepting it again, a spend-proof is posted to the public Byteball database. Upon receiving the blackbyte your full-wallet/light-wallet-hub provider, checks the spend-proof. The spend-proof does not contain amounts or sender/receiver information, it is merely a signature.

## Blackbyte Exchange

If blackbytes are traded on a centralized exchange, the exchange would have enough information to reveal the origin and destination of blackbytes, and amounts traded, which defeats their purpose.  
Blackbytes hence can only be traded on decentralized exchanges, with conditional payments in the Byteball wallet. Peer discovery has to happen outside the Byteball wallet. Such an exchange is yet to appear (2017-May).

## Recommended usage

1. Start a light wallet on one of your computers/smartphones, do not use revealing device name for the wallet, pick something like localhost or byteball.  
2. Enable Tor in the Global settings menu, this requires that you have tor installed or Orbot on Android, a tor-proxy for any Android app.  
3. Send your blackbytes to this wallet, and use it to trade with and pay for goods and services.  



-----
**This "Blackbytes" page main author: @portabella at the** [Byteball slack](http://slack.byteball.org/)

